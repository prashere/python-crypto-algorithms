#!/usr/bin/env python

"""
This small peice of code is a free software (GNU GPLv3).
You are free to,
 - Use this code for any purpose.
 - Study this code to understand.
 - Modify any line of this code to fit your requirements.
 - Distribute / redistribute to anyone with the same freedom that I give you.

Given a message in plain text this small piece of code will apply the
caeser cipher technique to produce a ciphertext in order to encrypt
the message.
"""

# import necessary modules
from utils.cryptoutils import generate_dict, string

class CaeserCipher:
    key = 3    # fixed substitution key
    alphabet_dict = generate_dict('alpha')
    number_dict = generate_dict('number')

    def process_text(self, plaintext):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in plaintext if ch not in exclude)

    def encrypt(self, plaintext):
        plaintext = self.process_text(plaintext.lower())
        splitted_plaintext = list(plaintext)
        splitted_ciphertext = []
        for each_character in splitted_plaintext:
            try:
                plain_number = self.alphabet_dict.get(each_character)
                cipher_number = (plain_number + self.key) % len(self.alphabet_dict)
                splitted_ciphertext.append(self.number_dict.get(cipher_number))
            except TypeError:
                splitted_ciphertext.append(" ")
        self.ciphertext = "".join(splitted_ciphertext)
        return self.ciphertext

    def decrypt(self, ciphertext):
        ciphertext = self.process_text(ciphertext.lower())
        splitted_ciphertext = list(ciphertext)
        splitted_plaintext = []
        for each_character in splitted_ciphertext:
            try:
                cipher_number = self.alphabet_dict.get(each_character)
                plain_number = (cipher_number - self.key) % len(self.number_dict)
                splitted_plaintext.append(self.number_dict.get(plain_number))
            except TypeError:
                splitted_plaintext.append(" ")
        return "".join(splitted_plaintext)


if __name__ == "__main__":
    caeser_cipher = CaeserCipher()

    message = input("Enter plaintext: ")
    ciphertext = caeser_cipher.encrypt(message)
    print ("Ciphertext: %s" %ciphertext)

    message = input("Enter the encrypted message: ")
    plaintext = caeser_cipher.decrypt(message)
    print ("Plaintext: %s" %plaintext)
